#!/usr/bin/env python3.8
# coding: utf-8
from __future__ import print_function
from torch import nn
from parser import *
from logger import Logger
from amino_acid import *
from dataset import *
import sys
import torch



# Définition des constantes du modèle
EPOCHS          = 20
LR              = .01
MOMENTUM        = .9
HIDDEN_LAYERS   = 1
HIDDEN_DIM      = 64
BATCH_SIZE      = 128
SEQUENCE_LENGTH = 13
OUTPUT_DIM      = 3



class Net(nn.Module):
    """Classe contenant la structure du modèle pytorch pour l'analyse des 
    séquences de protéines.

    Attributs
    ----------
    lr: float
        Pas d'apprentissage
    hidden_layer: int
        Nombre de couches cachés
    hidden_dim: int
        Nombre d'unité par couche cachée
    batch_setup: dict( str, int / bool )
        Dictionnaire des paramètres du mini-batch.
        Ce dictionnaire contient notamment le nombre d'entrées par mini-batch 
        dans le processus d'apprentissage.
    seq_len: int
        Longueur d'une séquence d'acides aminés
    output_dim: int
        Dimension des données de sortie (trois en l'occurence ici puisque le 
        jeu de données ne présente que trois structures secondaires différentes)
    """

    def __init__(
        self,
        num_amino_acids,
        lr              = LR,
        momentum        = MOMENTUM, 
        hidden_layers   = HIDDEN_LAYERS, 
        hidden_dim      = HIDDEN_DIM,
        seq_len         = SEQUENCE_LENGTH
    ):
        super( Net, self ).__init__()
        self.lr             = lr
        self.momentum = momentum
        self.input_size     = seq_len * num_amino_acids
        self.output_dim     = OUTPUT_DIM
        self.hidden_dim     = hidden_dim
        self.hidden_layers  = hidden_layers

        self.fc1 = nn.Linear( self.input_size, self.hidden_dim )
        self.relu1 = nn.ReLU()
        self.fc2 = nn.Linear( self.hidden_dim, self.output_dim )
        self.relu2 = nn.ReLU()



    def forward(self, x):
        """Propagation des caractéristiques de l'individu x dans le réseau 
        (i.e. définition du système de feed-forward).

        Paramètre
        ----------
        x: torch.tensor
            Entrée de taille `self.input_size` composant le jeu d'entraînement,
            à propager dans le réseau.
        
        Retourne
        ----------
        torch.tensor
            Sortie filtrée selon la fonction de rectification linéaire 
            appliquée au vecteur de sortie (ici de dimension 3).
        """
        x = self.fc1( x )
        x = self.relu1( x )
        x = self.fc2( x )
        return self.relu2( x )



    def fire(self, train_dataloader, validation_dataloader, epochs):
        """Lance la procédure d'apprentissage du réseau.

        Paramètres
        ----------
        train_dataloader: torch.data.DataLoader
            Liste des protéines composant le jeu d'entraînement
        epochs: int
            Nombre maximum d'itération dans le processus d'apprentissage
        """
        Logger.info('Learning process fired!')

        criterion = nn.CrossEntropyLoss()
        optimizer = torch.optim.SGD(
            net.parameters(), 
            lr = self.lr, # valeur conseillée: 0.01
            momentum = self.momentum
        )
        # optimizer = torch.optim.Adam(
        #     net.parameters(), 
        #     lr = self.lr # valeur conseillée: 0.001
        # )



        for epoch in range( epochs ):
            for local_batch, (entry, target) in enumerate( train_dataloader ):
                optimizer.zero_grad()
                estimated_output = self(entry)
                loss = criterion(estimated_output, target)
                loss.backward()
                optimizer.step()

                if ( local_batch + 1 ) % BATCH_SIZE  == 0:
                    self.check( validation_dataloader, epoch )



    def check(self, validation_dataloader, epoch = None):
        """Vérifie la précision du modèle en fonction de sa configuration 
        courante.

        Paramètres
        ----------
        validation_dataloader: torch.data.DataLoader
            Liste des protéines composant le jeu de validation
        """
        self.eval()
        correct = 0
        with torch.set_grad_enabled( False ):
            for data, target in validation_dataloader:
                output = net(data)
                pred = output.argmax( dim = 1, keepdim = True )
                correct += pred.eq( target.view_as(pred) ).sum().item()
        Logger.update( '{}acc.: {:<.3f}%{}'.format(
            '' if epoch is None else '@{:<6} [ '.format( epoch ),
            100. * correct / len( validation_dataloader.dataset ),
            '' if epoch is None else ' ]'
        ) )

    def __str__(self):
       return 'Net {{ \n\t{:<13}: {}\n\t{:<13}: {}\n\t{:<13}: {}\n\t{:<13}: {}\n}}'.format(
            'lr', 
            self.lr,
            'input_size',
            self.input_size,
            'hidden_layers', 
            self.hidden_layers,
            'hidden_dim', 
            self.hidden_dim
        )



    def __repr__(self):
        return 'Model {{ "lr": {:.3f}, "input_size": {}, "hidden_layers": {}, "hidden_dim": {} }}'.format(
            self.lr,
            self.input_size,
            self.hidden_layers,
            self.hidden_dim
        )




if __name__ == "__main__":
    """Exemple d'utilisation du modèle :
    python src/net.py -train data/protein-secondary-structure.train -test data/protein-secondary-structure.test -v
    """
    parsed_args = parse_args()

    # On active le Logger si nécessaire
    Logger.verbose = parsed_args[ 'verbose' ]
    parsed_args.pop( 'verbose' )
    
    if 'seq_len' in parsed_args:
        seq_len = parsed_args[ 'seq_len' ]
        del parsed_args[ 'seq_len' ]
    else:
        seq_len = SEQUENCE_LENGTH

    if 'batch_size' in parsed_args:
        batch_size = parsed_args[ 'batch_size' ]
        del parsed_args[ 'batch_size' ]
    else:
        batch_size = BATCH_SIZE

    # Définition des paramètres du mini-batch
    minibatch_setup = {
        'batch_size': batch_size,
        'shuffle': True,
        'num_workers': 4
    }

    # Création des DataLoader pour l'entraînement du modèle
    amino_acid_map, proteins = parse_file( parsed_args[ 'train_dataset' ] )
    del parsed_args[ 'train_dataset' ]
    train_dataloader = torch.utils.data.DataLoader( 
        Dataset(
            proteins, 
            amino_acid_map, 
            seq_len
        ), 
        **minibatch_setup 
    )

    _, proteins = parse_file( parsed_args[ 'test_dataset' ] )
    del parsed_args[ 'test_dataset' ]
    test_dataloader = torch.utils.data.DataLoader( 
        Dataset(
            proteins, 
            amino_acid_map, 
            seq_len
        ), 
        **minibatch_setup 
    )

    # Récupération du nombre d'itérations maximum du processus d'apprentissage
    if 'epochs' in parsed_args:
        epochs = parsed_args[ 'epochs' ]
        del parsed_args[ 'epochs' ]
    else:
        epochs = EPOCHS

    # Création du modèle
    net = Net( len( amino_acid_map.keys() ), **parsed_args )
    Logger.info( 'Network configuration:\n{}'.format( net ) )
    Logger.empty_line()

    # Lancement de la procédure d'entraînement
    net.fire( train_dataloader, test_dataloader, epochs )

    Logger.empty_line()
    Logger.success( 'Done!' )
    net.check( test_dataloader )
    
