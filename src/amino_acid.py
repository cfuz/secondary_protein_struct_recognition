#!/usr/bin/env python3.8
# coding: utf-8
from logger import Logger
import enum
import torch



class SecondaryStructure(enum.Enum):
    AlphaHelix  = 'alpha-helix'
    BetaSheet   = 'beta-sheet'
    Coil        = 'coil'
    
    @staticmethod
    def from_string(string):
        if string in [ 'h', 'alpha-helix' ]:
            return SecondaryStructure.AlphaHelix
        elif string in [ 'e', 'beta-sheet' ]:
            return SecondaryStructure.BetaSheet
        elif string in [ '_', 'coil' ]:
            return SecondaryStructure.Coil
        else:
            Logger.error('"{}" can\'t be casted to SecondaryStructure type'.format(string))
            return None
    
    @staticmethod
    def to_classes():
        return [ secondary_structure.value for secondary_structure in SecondaryStructure ]

    def to_index(self):
        if self == SecondaryStructure.AlphaHelix:
            return 0
        elif self == SecondaryStructure.BetaSheet:
            return 1
        elif self == SecondaryStructure.Coil:
            return 2
    
    def to_data(self):
        return self.value



class PrimaryStructure(object):
    def __init__(self, nucleid_name, nucleid_id, n_nucleids):
        self.name       = nucleid_name
        self.id         = nucleid_id
        self.one_hot    = [
            1 if i == nucleid_id else 0 for i in range(n_nucleids)
        ]
    
    def __repr__(self):
        return 'PrimaryStructure {{ "name": {}, "id": {}, "one_hot": {} }}'.format(
            self.name,
            self.id, 
            self.one_hot
        )

    def __str__(self):
        return 'PrimaryStructure{{\n\t{:<7}: {}\n\t{:<7}: {}\n\t{:<7}: {}\n}}'.format(
            'name', 
            self.name,
            'id', 
            self.id,
            'one_hot',
            self.one_hot
        )



class AminoAcid(object):
    def __init__(self, primary, secondary):
        self.primary = primary
        self.secondary = secondary
    
    def __repr__(self):
        return 'AminoAcid {{ "primary": {}, "secondary": {} }}'.format(
            repr(self.primary), 
            repr(self.secondary)
        )

    def __str__(self):
        return 'AminoAcid {{\n\t{:<9}: {}\n\t{:<9}: {}\n}}'.format(
            'primary', 
            repr(self.primary),
            'secondary',
            repr(self.secondary)
        )
