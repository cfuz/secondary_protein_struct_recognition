#!/usr/bin/env python3.8
# coding: utf-8
import torch
from torch.utils import data



class Dataset(data.Dataset):
    """Structure définissant le jeu de données pour PyTorch. 
    Pour son utilisation dans le cadre de l'entraînement d'un modèle :
    https://stanford.edu/~shervine/blog/pytorch-how-to-generate-data-parallel

    Attributs
    ----------
    proteins: list( ( int, list( AminoAcid ) ) )
        Liste de couples ( longueur protéine, séquence d'acides aminés ).
    labels: list( list ( SecondaryStructure ) )
        Matrice des étiquettes associées à chacun des acides aminés 
        composant chacune des protéines.
    amino_acid_map: dict( str, PrimaryStructure )
            Dictionnaire des acides aminés présent dans le jeu de données.
    seq_len: int
        Longueur d'une séquence d'acides aminés.
    """
    def __init__( self, proteins, amino_acid_map, seq_len ):
        """Initialisation du jeu de données selon les protéines et la liste des
        structures secondaires (étiquettes) composant le jeu de données.

        Paramètres
        ----------
        proteins: list( ( int, list( AminoAcid ) ) )
            Liste de couples ( longueur protéine, séquence d'acides aminés ).
        amino_acid_map: dict( str, PrimaryStructure )
            Dictionnaire des acides aminés présent dans le jeu de données.
        seq_len: int
            Longueur d'une séquence d'acides aminés.
        """
        self.amino_acid_map = amino_acid_map
        self.seq_len = seq_len
        self.sequences, self.labels = self.into_sequences( proteins )
        self.sequences = torch.tensor( self.sequences, dtype = torch.float )
        self.labels = torch.tensor( self.labels )



    def __len__( self ):
        """Fonction intrinsèque à la classe retournant le nombre total d'entrées la composant

        Retourne
        ----------
        int
            Nombre d'individus composant le jeu de données
        """
        return len( self.labels )
    


    def __getitem__( self, index ):
        """Récupère un élèment du jeu de données selon son indice

        Paramètre
        ----------
        index: int
            Indice du couple ( entrée, étiquette ) à extraire
        
        Retourne
        ----------
        ( list( int ), int )
            Le couple ( séquence d'acides aminés encodés en one hot, indice de 
            la structure secondaire ) associé à l'indice fournit en paramètre.
        """
        return self.sequences[ index ], self.labels[ index ]



    def into_sequences( self, raw_proteins ):
        """Instancie une liste de séquences d'acides aminés de taille fixe à 
        partir d'une protéine extraites du jeu de données, ainsi que la liste
        des structures secondaires de tous les acides aminés composant les 
        protéines.

        Paramètres
        ----------
        raw_proteins: list( ( int, list( AminoAcid ) ) )
            Liste de couples ( longueur protéine, séquence d'acides aminés ).
            Il s'agit de la liste extraite directement du parser au lancement 
            de l'application.
        
        Retourne
        ---------- 
        labels: SecondaryStructure
            Structure secondaires associées à chaque acide aminés situés au 
            milieu des séquences extraites
        sequences: list( list( int ) )
            Liste des séquences de taille fixe, composée d'un enchaînement de 
            vecteurs de type "one hot" encodant la structure primaire de la 
            protéine.
        """
        sequences, labels = [], []
        for ( protein_len, protein ) in raw_proteins:
            l, s = self.cut( protein_len, protein )
            sequences += s 
            labels += l
        return sequences, labels
    


    def cut(self, protein_len, protein):
        """Coupe une protéine en séquences d'acides aminés de même taille.
        
        Paramètres
        ----------
        protein_len: int
            Longueur de la protéine
        protein: list( AminoAcid )
            Séquence d'acides aminés constituant ladite protéine
        
        Retourne
        ---------- 
        labels: SecondaryStructure
            Structure secondaires associées chaque acide aminés situés au 
            milieu des séquences extraites
        sequences: list( list( int ) )
            Liste des séquences de taille fixe, composée d'un enchaînement de 
            vecteurs de type "one hot" encodant la structure primaire de la 
            protéine.
        """
        num_amino_acids = len( self.amino_acid_map.keys() )
        sequences, labels = [], []
        empty = [ 0 for _ in range( num_amino_acids ) ]

        for i, amino_acid in enumerate( protein ):
            sequence = []

            for j in range( int( self.seq_len / 2 ), 0, -1 ):
                sequence += empty if i - j < 0 else protein[ i - j ].primary.one_hot
            
            sequence += amino_acid.primary.one_hot
            labels += [ amino_acid.secondary.to_index() ]

            for j in range( 1, int( self.seq_len / 2 ) + 1 ):
                sequence += empty if i + j >= protein_len else protein[ i + j ].primary.one_hot
                        
            sequences += [ sequence ]

        return labels, sequences
