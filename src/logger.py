#!/usr/bin/env python3.8
# coding: utf-8



class Logger:
    """Classe gérant l'affichage des logs de l'application. Si son attribut 
    `verbose` est à False, aucun des messages qui lui seront transmis ne seront
    affichés.

    Attributs
    ----------
    verbose: bool
        Définit le caractère explicite de l'application
    """
    verbose = False

    def __repr__(self):
        return 'Logger {{ @static "verbose": {} }}'.format(
            Logger.verbose,
        )

    @staticmethod
    def empty_line():
        if Logger.verbose:
            print()

    @staticmethod
    def info(message):
        if Logger.verbose:
            print("➜  {}".format(message))
    
    @staticmethod
    def subinfo(message):
        if Logger.verbose:
            print("   {}".format(message))

    @staticmethod
    def subsubinfo(message):
        if Logger.verbose:
            print("      {}".format(message))

    @staticmethod
    def success(message):
        if Logger.verbose:
            print("✔  {}".format(message))
    
    @staticmethod
    def update(message):
        if Logger.verbose:
            print("ϟ  {}".format(message))
    
    @staticmethod
    def warning(message):
        if Logger.verbose:
            print("☛  {}".format(message))
    
    @staticmethod
    def error(message):
        if Logger.verbose:
            print("✘  {}".format(message))
