#!/usr/bin/env python3.8
# coding: utf-8
import argparse
from amino_acid import *



def parse_args():
    """Parse les arguments entrés par l'utilisateur.

    Paramètres
    ----------
    args: list( str )
        Liste des arguments entrés par l'utilisateur au lancement de l'application.
    
    Retourne
    ----------
    parsed_args: dict( str, int|float|str )
        Dictionnaire des arguments parsés.
    """
    parser = argparse.ArgumentParser()

    # Récupération des arguments nécessaires
    parser.add_argument(
        '-train', '--train_dataset',
        required = True,
        type = str,
        help = 'File containing the training dataset'
    )
    parser.add_argument(
        '-test', '--test_dataset',
        required = True,
        type = str,
        help = 'File containing the test dataset'
    )

    # Parsing des arguments optionnels
    parser.add_argument(
        '-lr', '--learning_rate',
        type = float,
        help = 'Learning rate of each unit composing the model (default: 0.01)'
    )
    parser.add_argument(
        '-m', '--momentum',
        type = float,
        help = 'Model\'s momentum (default: 0.9)'
    )
    parser.add_argument(
        '-e', '--epochs',
        type = int,
        help = 'Maximum number of iterations used for the learning process (default: 20)'
    )
    parser.add_argument(
        '-sl', '--seq_len',
        type = int,
        help = 'Length of the amino acid sequence to learn from (default: 13)'
    )
    parser.add_argument(
        '-hl', '--hidden_layers',
        type = int,
        help = 'Number of hidden layers composing the model (default: 1)'
    )
    parser.add_argument(
        '-hd', '--hidden_dim',
        type = int,
        help = 'Number of logic units per hidden layer (default: 64)'
    )
    parser.add_argument(
        '-bs', '--batch_size',
        type = int,
        help = 'Number of processed sequences per iteration (default: 128)'
    )
    parser.add_argument(
        '-v', '--verbose',
        action = 'store_true',
        help = "Starts the script in explicit mode"    
    )
    
    # Transposition des arguments récupérés en dictionnaire
    args = parser.parse_args()
    opts = vars( args )
    return { key: opts[ key ] for key in opts if opts[ key ] is not None }



def parse_file( filename ):
    """Extrait les données d'un fichier selon son nom.

    Paramètre
    ----------
    filename: str
        Nom du fichier à parser.
    
    Retourne
    ----------
    amino_acids_map: dict( str, PrimaryStructure )
        Dictionnaire des acides aminés présent dans le jeu de données.
    proteins: list( ( int, list( AminoAcid ) ) )
        Liste de couples ( longueur protéine, séquence d'acides aminés ).
    """
    recording   = False
    protein_id  = -1
    amino_acids, proteins = [], []

    file        = open( filename, "r" )

    for line in file:
        buffer  = line.split( '\n' )[ 0 ]

        if buffer in [ '<>', '<end>', 'end', '' ]:
            if buffer == '<>':
                proteins    += [ [] ]
                protein_id  += 1
                recording   = True
            continue

        if recording:
            buffer      = buffer.split()
            amino_acid  = buffer[ 0 ]
            if amino_acid not in amino_acids:
                amino_acids.append( amino_acid )
            proteins[ protein_id ] += [ (
                amino_acid, 
                buffer[ 1 ]
            ) ]

    file.close()

    amino_acids_map = get_amino_acid_map( amino_acids )
    proteins = get_dataset( proteins, amino_acids_map )

    return amino_acids_map, proteins



def get_amino_acid_map( amino_acids ):
    """Récupère le dictionnaire des acides aminés à partir d'une liste 
    d'acides aminés fournie en paramètres.

    Paramètre
    ----------
    amino_acids: list( str )
        Liste des acides aminés.

    Retourne
    ----------
    dict( str, PrimaryStructure )
        Dictionnaire des acides acides aminés.
    """
    return {
        amino_acid: PrimaryStructure(
            amino_acid,
            index,
            len( amino_acids )
        ) for index, amino_acid in enumerate( sorted( amino_acids ) )
    }



def get_dataset( proteins, amino_acids_map ):
    """Première mise en forme du jeu de données sous forme de liste de couples
    ( longueur de protéine, séquence d'acides aminés ), créée à partir d'une 
    liste de protéines présentées sous forme de string et du dictionnaire 
    des acides aminés.

    Paramètres
    ----------
    proteins: list( str )
        Liste brute des protéines, présentées sous forme de strings.
    amino_acids_map: dict( str, PrimaryStructure )
        Dictionnaire des acides aminés.

    Retourne
    ----------
    list( int, list( AminoAcid ) )
        Liste de couples ( longueur de protéine, séquence d'acides aminés ).
    """
    return [ (
        len(protein),
        [ 
            AminoAcid(
                amino_acids_map[amino_acid[0]],
                SecondaryStructure.from_string(amino_acid[1])
            ) for amino_acid in protein 
        ]
    ) for protein in proteins ]




if __name__ == '__main__':
    print( parse_args() )