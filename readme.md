# Application IA - Jonathan Heno, Adam Serghini

Etudiants | Date | Sujet
:---|:---|:---
J. Heno et A. Serghini | 08/04/2020 | Prédiction de structures protéiques secondaires via des modèles neuronaux.


# Introduction
L'unité complémentaire d'enseignement **Application IA**, a pour objectif de nous présenter les rudiments des réseaux de neurones.
Notamment, il s'agira de nous faire explorer un ou des cadriciels largement répandus dans le domaine, dans l'optique de répondre à un problème donné.
Ainsi, dans le cadre expérimental énoncé par l'article [Qian, Ning, and Terrence J. Sejnowski. "Predicting the secondary structure of globular proteins using neural network models." Journal of molecular biology 202.4 (1988): 865-884](https://www.sciencedirect.com/science/article/pii/0022283688905645), nous proposons ici une implémentation d'un modèle neuronal prédisant la structure secondaire des acides aminés composant des protéines globulaires.

Le développement de la solution a été réalisé dans l'enceinte de l'environnement [Pytorch](https://pytorch.org/), une bibliothèque Python open source, véloce et précisément conçue pour répondre à ce type de problématique.

# Avant-propos
## Installation des dépendances et préconisations
Afin que vous disposiez du même environnement de travail que le notre dans le cadre de ce projet, l'ensemble des dépendances ont été inscrites dans le fichier `requirements.txt`.
Par ailleurs, la solution s'avère fonctionnelle sous **Python 3.8.2**.

## Documentation
Afin que vous puissiez comprendre notre démarche, nous avons pris le parti de fournir une **documentation quasi-complète** de notre code source.
C'est pourquoi nous ne nous pencherons que très peu sur cette partie.

## Utilisation
Le script `src/net.py` est le point d'entrée de l'application.
Son utilisation est définit comme suit :

```bash
usage: net.py [-h] -train TRAIN_DATASET -test TEST_DATASET [-lr LEARNING_RATE]
              [-m MOMENTUM] [-e EPOCHS] [-sl SEQ_LEN] [-hl HIDDEN_LAYERS]
              [-hd HIDDEN_DIM] [-bs BATCH_SIZE] [-v]
```

La solution nécessite que l'on fournisse en entrée les éléments suivants :
```
-train TRAIN_DATASET, --train_dataset TRAIN_DATASET
                      File containing the training dataset
-test TEST_DATASET, --test_dataset TEST_DATASET
                      File containing the test dataset
```

Si vous souhaitez définir un réglage plus fin du modèle nous avons également défini les options suivantes :
```
-lr LEARNING_RATE, --learning_rate LEARNING_RATE
                      Learning rate of each unit composing the model
                      (default: 0.01)
-m MOMENTUM, --momentum MOMENTUM
                      Model's momentum (default: 0.9)
-e EPOCHS, --epochs EPOCHS
                      Maximum number of iterations used for the learning
                      process (default: 20)
-sl SEQ_LEN, --seq_len SEQ_LEN
                      Length of the amino acid sequence to learn from
                      (default: 13)
-hl HIDDEN_LAYERS, --hidden_layers HIDDEN_LAYERS
                      Number of hidden layers composing the model (default:
                      1)
-hd HIDDEN_DIM, --hidden_dim HIDDEN_DIM
                      Number of logic units per hidden layer (default: 64)
-bs BATCH_SIZE, --batch_size BATCH_SIZE
                      Number of processed sequences per iteration (default:
                      128)
-v, --verbose         Starts the script in explicit mode
```

À titre d'exemple :
```bash
$ python src/net.py -train data/protein-secondary-structure.train -test data/protein-secondary-structure.test -lr 0.001 -m 0.8 -sl 11 -bs 64 -e 30 -v
```


À noter que, les arguments passés pour les options `-train` et `-test` doivent indiquer les chemins des fichiers contenant, respectivement, les données du jeu d'entraînement et du jeu de test (qui fera office de jeu de validation dans notre situation).
Par ailleurs, les valeurs affichées par défaut sont celles que nous avons jugé adéquate pour un résultat optimal.


# Traitement des données d'entrée
Une des parties les plus importantes et la plus longue, dans le processus de développement du modèle, a été pour nous la constitution des corpus de données.
Celle-ci se déroule en deux phases :

## Analyseur de fichier
*c.f.* `src/parser.py`

Dans un premier temps, nous avons du extraire les données inscrites dans les fichiers d'entrée.
L'un des problèmes majeur dans cette étape, fut pour nous de gérer la relative hétérogénéité des informations présentées.
En effet, parfois des balises sensées signaler la fin d'une séquence d'acides aminés (*i.e.* d'une protéine) ne parraissaient pas et faussait par conséquent le jeu de protéines que nous devions obtenir en sortie.

Outre cela, nous avons décidé de définir le jeu de données en sortie d'extraction comme une liste de couples, dont la première composante représente le **nombre d'acides aminés** composant la protéine et la seconde, la **liste des acides aminés** la constituant.
À des fins explicites nous avons décidé de définir, les structures suivantes :
* Une énumération des différentes structures secondaires codées 'en dur' : 
```python
class SecondaryStructure(enum.Enum):
    AlphaHelix  = 'alpha-helix'
    BetaSheet   = 'beta-sheet'
    Coil        = 'coil'
    # ...
```

* Une classe représentant la structure primaire de l'acide aminé
```python
class PrimaryStructure(object):
    def __init__(self, nucleid_name, nucleid_id, n_nucleids):
        self.name       = nucleid_name
        self.id         = nucleid_id
        self.one_hot    = [ 1 if i == nucleid_id else 0 for i in range( n_nucleids ) ]
    # ...
```
Un dictionnaire, créé en fin de phase d'extraction des chaînes, permet d'instancier ces entités selon un référentiel commun.
Cette alternative nous permet notamment de travailler sur des vocabulaires plus étendus sans vraiment nous soucier de leur contenu.

* Une classe composite caractérisant l'acide aminé en lui-même :
```python
class AminoAcid(object):
    def __init__(self, primary, secondary):
        self.primary = primary
        self.secondary = secondary
    # ...
```

Se faisant, il nous est donc possible de récupérer aisément toutes les informations nécessaires concernant un acide aminés en interrogeant directement ses attributs.
Fort de ces structures, nous pouvions donc passer à la phase suivante.

## Conversions
*c.f.* `src/dataset.py`

Nous avons ici pris le parti d'exploiter au maximum les ressources qui nous sont mises à disposition dans la bibliothèque `pytorch`. Ainsi, et sur conseil de Jarod Duret, nous avons décidé de transformer les données précédemment traités en `Dataset`, afin de nous faciliter la gestion des mini-batch, lors de la phase d'apprentissage du modèle.
Il s'est avéré, en effet, que l'on s'est confronté à des problèmes de dimensionnalités, lorsque l'on se contentait d'injecter directement une matrice d'entrées, constituées de séquences d'acides aminés encodées en **one hot**, couplée à la liste des étiquettes correspondante. 
La spécialisation de la classe `torch.utils.data.Dataset` nous a permis de nous affranchir de ces soucis.
Il s'avère en outre que cette classe est parfaitement conçue pour la gestion de larges volumes de données en mémoire, caractéristique très intéressante en ce qui concerne la question du passage à l'échelle d'un modèle.

Nous avons donc suivi [ce tutoriel](https://stanford.edu/~shervine/blog/pytorch-how-to-generate-data-parallel) pour l'intégration de `Dataset` personnalisés.
Gageons de préciser que c'est lors de l'instanciation de cette classe que nous générons nos séquences d'acides-aminés, encodés en fonction de la liste des protéines extraites lors de la phase précédente.
Cette conversions des données se fait via la méthode `into_sequences()`.
Afin de disposer de séquences de taille identique, nous avons comblés les manques par des acides-aminés nuls (encodés par un vecteur nul), ce que certains appellent le `zero-padding`.
Quand aux étiquettes, celles-ci sont récupérées et stockées sous forme d'indices d'équivalence de l'enumération `SecondaryStructure` explicitée plus-haut.

Inscrite dans un `torch.utils.data.DataLoader` comme suit :
```python
# Définition des paramètres du mini-batch
minibatch_setup = {
    'batch_size': batch_size,
    'shuffle': True,
    'num_workers': 4
}

# Création du DataLoader pour l'entraînement du modèle
amino_acid_map, proteins = parse_file( parsed_args[ 'train_dataset' ] )
train_dataloader = torch.utils.data.DataLoader( 
    Dataset(
        proteins, 
        amino_acid_map, 
        seq_len
    ), 
    **minibatch_setup 
)
```
Son exploitation au sein du modèle est alors grandement simplifiée et surtout bien plus optimisée que notre solution *a priori*.


# Implémentation du modèle
*c.f.* `src/net.py`

Pour le développement de notre modèle, nous étions partis sur l'idée d'utiliser un réseau de neurones récurrent de type **LSTM** (Long Short-Term Memory).
En effet, ce type d'architecture semble être bien adapté à des problèmes d'analyse de séquences tel que le notre.

## Il faut parfois savoir rester simple...
Cependant, à force de documentation et de recherche, nous avons préféré partir sur un modèle plus basique. En effet, ce projet étant notre première réalisation dans le domaine de l'intelligence artificielle, nous avons éprouvé beaucoup de difficultés pour comprendre le fonctionnement de `PyTorch`. 
Il nous a semblé en effet que, malgré le nombre pléthorique de blogs et de revues d'implémentation de réseaux de neurones dans ce paradigme logiciel, il était difficile de trouver des explications qui nous semblaient claires sur des exemples simples. Nous avons donc décidé de nous orienté vers les fondamentaux, en optant pour un **perceptron multicouche**. Cela nous a ainsi permis de traiter notre problème plus simplement et efficacement, tout en nous familiarisant avec les fonctions primaires du cadriciel.

## La propagation de l'information dans le réseau
Ainsi, la couche d'entrée est liée de façon linéaire aux neurones de la couche intermédiaire/cachée dont les sorties sont filtrées par une première unité de rectification linéaire : elle constitue la fonction d'activation de notre couche cachée.
Entre cette dernière et la couche de sortie, un opérateur linéaire réalise le liant.
Enfin une autre filtrage par rectification linéaire est appliqué, afin d'en extraire la (ou les) réponses que le système juge la (ou les) plus probable(s).

## Les modules d'analyse et d'optimisation
Le lancement du processus d'apprentissage se fait dans l'enceinte de la méthode `fire()` du modèle.
Dans celle-ci, nous définissons deux variables aux commandes de la gestion de la configuration du réseau et de ses réponses : le `criterion` et l'`optimizer`.

La première permet de jauger l'éloignement de la prédiction en regard de la sortie attendue.
De nombreuses instances permettent d'apporter et d'analyser différemment ces écarts au réel, notre choix s'est donc porté sur la classe `torch.nn.CrossEntropyLoss`, toute indiquée pour des problèmes de classification. Cette fonction d'entropie croisée pénalise les mauvaises prédictions avec un haut taux de confiance mais aussi, a contrario, les bonnes prédictions avec un faible taux de confiance.
De plus, un softmax est encapsulé dans ce type d'opérateur, nous permettant ainsi de récupérer directement la distribution de probabilité associée à l'estimation de notre sortie

Par ailleurs, la fonction de l'`optimizer` consiste à optimiser la manière dont sont mis à jour les poids du réseau. Également, dans le registre, la bibliothèque `PyTorch` ne manque pas de ressources.
Nous avons par conséquent décidés d'appliquer des méthodes classiques telles que la descente de gradient stochastique `torch.optim.SGD`, et une autre méthode d'optimisation stochastique, `torch.optim.Adam`. Des fonctions qui semblent somme toute largement répandues sur les différents forums et blogs que nous avons pu visiter.
Ci-dessous les valeurs des hyperparamètres et des résultats obtenus avec ces fonctions :

|                               | **SGD**  | **Adam** |
|-------------------------------|:--------:|:--------:|
| **Learning Rate**             |   0.01   |   0.001  |
| **Unités par couche cachée**  |    64    |   128    |
| **Nb. itérations max.**       |    20    |   10     |
| **Momentum**                  |   0.9    |   -      |
| **Résultat**                  |   63.36% |   63.92% |

Nous avons souhaité essayer de confronter ces deux méthodes dans notre projet afin d'analyser s'il existait des différences notables de comportement en sortie de modèle.
Comme l'on peut le constater les résultats sont relativements proches après avoir déterminé une bonne configuration pour chacune des méthodes, bien qu'un léger avantage soit donné à `Adam` du fait de sa précision et de sa rapidité de convergence plus élevées.
À noter que chaque fonction requiert un ajustement bien particulier pour produire des résultats satisfaisant. Il y a donc ici une forte emprunte empirique dans le processus de modélisation.

Finalement, lors du processus d'apprentissage, nous interrogeons périodiquement le modèle sur sa capacité de généralisation en évaluant ses performances (*i.e.* son taux d'erreurs) sur le jeu de test (qui fait ici office de jeu de validation).

Lors du développement de cette partie de la solution, des problèmes de dimensions et des conflits de type ont principalement été relevés et surtout lors de la phase susmentionnée.
